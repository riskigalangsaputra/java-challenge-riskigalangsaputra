package ist.challenge.riski_galang_saputra;

import ist.challenge.riski_galang_saputra.entity.User;
import ist.challenge.riski_galang_saputra.service.IntegrationServiceImplement;
import lombok.NonNull;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import javax.validation.constraints.NotNull;
import java.text.DecimalFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

@SpringBootTest
class RiskiGalangSaputraApplicationTests {

    @Autowired
    private IntegrationServiceImplement service;

    @Test
    void cobApi() {
        service.coba();
    }

    @Test
    void formatStringDecimal() {
        long myLong = 12345L; // Your long number

        String formattedNumber = String.format("%d.00", myLong);
        System.out.println(formattedNumber);
    }

    @Test
    void iso8601() {
        LocalDateTime currentDateTime = LocalDateTime.now();

        // Formatting the current timestamp to ISO 8601 format
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss+07:00");
        String iso8601Format = currentDateTime.format(formatter);

        System.out.println("ISO 8601 Timestamp: " + iso8601Format); /*  */
    }

    @Test
    void prePersist() {
        String numberString = "2001100";

        String firstTwoDigits = numberString.substring(0, Math.min(numberString.length(), 3));
        System.out.println("First two digits: " + firstTwoDigits);

        int length = numberString.length();
        String lastTwoDigits = (length >= 2) ? numberString.substring(length - 2) : numberString;
        System.out.println("Last two digits: " + lastTwoDigits);
    }

    private static boolean status(@NotNull String responCode) {
        String httpCode = responCode.substring(0, Math.min(responCode.length(), 3));
        int length = responCode.length();
        String caseCode = (responCode.length() >= 2) ? responCode.substring(length - 2) : responCode;
        if ("200".equals(httpCode) && "00".equals(caseCode)) {
            return true;
        }
        return false;
    }

    @Test
    void contextLoads() {
        var trx = "99020800000045785";
        Optional<String> product = Optional.empty();
        List<String> numbers = Arrays.asList("PREPAID", "NONTAGLIS", "POSTPAID");


//		if (trx.equals("PREPAID")) {
//			product = numbers.stream().filter(val -> val.equals("PREPAID")).map(String::toString).findFirst();
//			System.out.println(",masuk PREPAID");
//		}
//
//		if (trx.equals("NONTAGLIS")) {
//			product = numbers.stream().filter(val -> val.equals("PREPAID")).map(String::toString).findFirst();
//			System.out.println("masuk NONTAGLIS");
//		}
//
//        product = numbers.stream().filter(val -> val.equals(getTrxPlnCategory(trx).name())).map(String::toString).findFirst();
//
//
//        if (!product.isPresent()) {
//            System.out.println("Product not found");
//        } else {
//            System.out.println("success : " + product.get());
//        }
    }

//    public static TrxPlnCategory getTrxPlnCategory(@NonNull String idTrx) {
//        if (idTrx == null) {
//            throw new NullPointerException("idTrx is marked non-null but is null");
//        } else {
//            switch (idTrx.substring(5, 6)) {
//                case "1":
//                    return TrxPlnCategory.PREPAID;
//                case "2":
//                    return TrxPlnCategory.POSTPAID;
//                case "3":
//                    return TrxPlnCategory.NONTAGLIS;
//                case "4":
//                    return TrxPlnCategory.NONTAGLIS_PAKET;
//                default:
//                    return TrxPlnCategory.NONPLN;
//            }
//        }
//    }

    private enum TrxPlnCategory {
        PREPAID,
        POSTPAID,
        NONTAGLIS,
        NONTAGLIS_PAKET,
        NONPLN;

        private TrxPlnCategory() {
        }
    }
}
