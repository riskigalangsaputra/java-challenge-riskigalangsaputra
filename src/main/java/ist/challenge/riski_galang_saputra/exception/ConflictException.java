package ist.challenge.riski_galang_saputra.exception;

/*
 * @Author  : galang
 * @Created : 17-11-2023
 * @Email   : riski.saputra@iconpln.co.id
 */

public class ConflictException extends RuntimeException {

    public ConflictException(String message) {
        super(message);
    }
}
