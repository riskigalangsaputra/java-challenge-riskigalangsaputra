package ist.challenge.riski_galang_saputra.exception;

/*
 * @Author  : galang
 * @Created : 17-11-2023
 * @Email   : riski.saputra@iconpln.co.id
 */

public class UnAuthorizedException  extends RuntimeException {

    public UnAuthorizedException(String message) {
        super(message);
    }
}
