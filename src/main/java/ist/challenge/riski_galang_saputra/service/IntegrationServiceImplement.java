package ist.challenge.riski_galang_saputra.service;

/*
 * @Author  : galang
 * @Created : 17-11-2023
 * @Email   : riski.saputra@iconpln.co.id
 */

import com.google.gson.Gson;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.binary.Hex;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import java.nio.charset.StandardCharsets;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.Map;

@Service
@RequiredArgsConstructor
@Slf4j
public class IntegrationServiceImplement implements IntegrationService {

    private final RestTemplate restTemplate;

    @Value("${integration.api.url}")
    private String url;

    @Override
    public Map<String, Object> findAllPepople() {

        ResponseEntity<Map<String, Object>> responseEntity;
        Map<String, Object> responsMap;
        var gson = new Gson();


        UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(url)
                .queryParam("param1", "nilai1")
                .queryParam("param2", "nilai2");

        String fullUrl = builder.toUriString();

        try {
            responseEntity = restTemplate.exchange(fullUrl, HttpMethod.GET,
                    new HttpEntity<>(createHeader()),
                    new ParameterizedTypeReference<>() {
                    });
            log.info("## Response : {}", gson.toJson(responseEntity.getBody()));
            responsMap = responseEntity.getBody();
            return responsMap;
        } catch (Exception e) {
            e.printStackTrace();
            return new HashMap<>();
        }
    }

    public Map<String, Object> coba() {
        ResponseEntity<Map<String, Object>> responseEntity;
        Map<String, Object> responsMap;
        var gson = new Gson();

        try {

            UriComponentsBuilder builder = UriComponentsBuilder.fromUriString("http://10.14.204.176:7071/apispkludev/api/webportal/get_spklu")
                    .queryParam("idPartner", 2);

            String fullUrl = builder.toUriString();
            responseEntity = restTemplate.exchange(fullUrl, HttpMethod.GET,
                    new HttpEntity<>(createHeader()),
                    new ParameterizedTypeReference<>() {
                    });
            log.info("## Response : {}", gson.toJson(responseEntity.getBody()));
            responsMap = responseEntity.getBody();
            return responsMap;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    private static HttpHeaders createHeader() {
        return new HttpHeaders() {{
            val timesTamp = String.valueOf(System.currentTimeMillis());
            val signature = generateHmac256Hex("tidaktau", timesTamp);

            log.info("## timesTamp request : {}", timesTamp);
            log.info("## signature request : {}", signature);

            set("Content-Type", "application/json");
            set("X-Timestamp", timesTamp);
            set("X-Client-Id", "tidaktau");
            set("X-Content-Signature", signature);
        }};
    }

    /*
    private ParamsHmacChargein checkHmac() {
        Long timeStamp = System.currentTimeMillis();
        ParamsHmacChargein dto = new ParamsHmacChargein();
        dto.setTimeStamp(timeStamp);
        dto.setSignature(generateHmac256Hex(keySecretHmac, timeStamp.toString()));
        dto.setClientId(clientIdHmac);
        return dto;
    }

     */

    public static String generateHmac256Hex(String key, String message) {
        return Hex.encodeHexString(
                generateHmac256Byte(key.getBytes(StandardCharsets.UTF_8), message.getBytes(StandardCharsets.UTF_8))
        );
    }

    public static byte[] generateHmac256Byte(byte[] key, byte[] message) {
        try {
            Mac mac = Mac.getInstance("HmacSHA256");
            mac.init(new SecretKeySpec(key, "HmacSHA256"));
            return mac.doFinal(message);
        } catch (NoSuchAlgorithmException | InvalidKeyException e) {
            e.printStackTrace();
        }
        return new byte[]{};
    }

}
