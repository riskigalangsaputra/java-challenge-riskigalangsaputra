package ist.challenge.riski_galang_saputra.service;

/*
 * @Author  : galang
 * @Created : 17-11-2023
 * @Email   : riski.saputra@iconpln.co.id
 */

import ist.challenge.riski_galang_saputra.dao.UserDao;
import ist.challenge.riski_galang_saputra.entity.User;
import ist.challenge.riski_galang_saputra.exception.BadRequestException;
import ist.challenge.riski_galang_saputra.exception.ConflictException;
import ist.challenge.riski_galang_saputra.exception.UnAuthorizedException;
import ist.challenge.riski_galang_saputra.model.LoginRequest;
import ist.challenge.riski_galang_saputra.model.RegisterRequest;
import ist.challenge.riski_galang_saputra.utils.PasswordUtils;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@RequiredArgsConstructor
@Slf4j
public class UserServiceImplement implements UserService {

    private final UserDao userDao;

    @Override
    public String login(LoginRequest request) {
        var user = userDao.findByUsername(request.getUsername()) //
                .orElseThrow(() -> new UnAuthorizedException("Username atau password salah 1"));

        if (!user.getPassword().equals(PasswordUtils.encodeBase64(request.getPassword()))) {
            throw new UnAuthorizedException("Username atau password salah 2");
        }

        return "Sukses Login";
    }

    @Override
    public List<User> getList() {
        return userDao.findAll();
    }

    @Override
    @Transactional
    public String update(User userRequest) {
        log.info(">> User request : {}", userRequest);
        var user = userDao.findById(userRequest.getId()).orElseThrow(() -> new BadRequestException("User tidak ditemukan"));

        if (user.getUsername().equals(userRequest.getUsername())) {
            throw new ConflictException("Username sudah terpakai");
        }

        if (PasswordUtils.decodeBase64(user.getPassword()).equals(userRequest.getPassword())) {
            throw new BadRequestException("Password tidak boleh sama dengan password sebelumnya");
        }

        user.setUsername(userRequest.getUsername());
        user.setPassword(PasswordUtils.encodeBase64(userRequest.getPassword()));
        userDao.save(user);

        return "sukses";
    }

    @Override
    public User getUserId(Long id) {
        return userDao.findById(id).orElseThrow(() -> new RuntimeException("User tidak ditemukan"));
    }

    @Override
    @Transactional
    public String register(RegisterRequest userRequest) {

        var userDb = userDao.existsByUsername(userRequest.getUsername());
        if (userDb) {
            throw new ConflictException("Username sudah terpakai");
        }

        var user = new User();
        user.setUsername(userRequest.getUsername());
        user.setPassword(PasswordUtils.encodeBase64(userRequest.getPassword()));
        userDao.save(user);
        return "sukses";
    }
}
