package ist.challenge.riski_galang_saputra.service;

/*
 * @Author  : galang
 * @Created : 17-11-2023
 * @Email   : riski.saputra@iconpln.co.id
 */

import ist.challenge.riski_galang_saputra.entity.User;
import ist.challenge.riski_galang_saputra.model.LoginRequest;
import ist.challenge.riski_galang_saputra.model.RegisterRequest;

import java.util.List;

public interface UserService {
    String login(LoginRequest request);

    List<User> getList();

    String update(User userRequest);

    User getUserId(Long id);

    String register(RegisterRequest userRequest);
}
