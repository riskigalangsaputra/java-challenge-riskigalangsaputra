package ist.challenge.riski_galang_saputra.service;

/*
 * @Author  : galang
 * @Created : 18-11-2023
 * @Email   : riski.saputra@iconpln.co.id
 */

import java.util.Map;

public interface IntegrationService {
    Map<String, Object> findAllPepople();
}
