package ist.challenge.riski_galang_saputra.utils;

/*
 * @Author  : galang
 * @Created : 17-11-2023
 * @Email   : riski.saputra@iconpln.co.id
 */

import org.springframework.beans.factory.annotation.Value;

public interface ConstantUtils {

    public enum Setting {
        CONNECTION_REQUEST_TIMEOUT(30000),
        CONNECTION_TIMEOUT(30000),
        READ_TIMEOUT(30000);

        private final int value;

        Setting(int value) {
            this.value = value;
        }

        public int value() {
            return value;
        }
    }
}
