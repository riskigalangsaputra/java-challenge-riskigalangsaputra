package ist.challenge.riski_galang_saputra.utils;

/*
 * @Author  : galang
 * @Created : 17-11-2023
 * @Email   : riski.saputra@iconpln.co.id
 */

import lombok.extern.slf4j.Slf4j;

import java.util.Base64;

@Slf4j
public class PasswordUtils {

    public static String encodeBase64(String password) {
        return Base64.getEncoder().encodeToString(password.getBytes());
    }

    public static String decodeBase64(String password) {
        try {
            byte[] decodedBytes = Base64.getDecoder().decode(password);
            String decodedString = new String(decodedBytes);

            log.info("Decoded String: {}" + decodedString);
            return decodedString;
        } catch (IllegalArgumentException e) {
            log.error("Invalid Base64 format: {}" + e.getMessage());
            return null;
        }
    }
}
