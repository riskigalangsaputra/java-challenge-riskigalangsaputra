package ist.challenge.riski_galang_saputra.controller;

/*
 * @Author  : galang
 * @Created : 17-11-2023
 * @Email   : riski.saputra@iconpln.co.id
 */

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import ist.challenge.riski_galang_saputra.exception.BadRequestException;
import ist.challenge.riski_galang_saputra.model.LoginRequest;
import ist.challenge.riski_galang_saputra.service.UserService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@Api(value = "Authentification", description = "Login User", tags = {"Authentification"})
@Slf4j
public class AuthController {

    private final UserService userService;

    @PostMapping("/api/auth/login")
    @ApiOperation(value = "Login user")
    public ResponseEntity<?> login(@RequestBody LoginRequest request) {
        if (request.getUsername() == null || request.getUsername().isEmpty() || request.getPassword() == null || request.getPassword().isEmpty()) {
            throw new BadRequestException("Username dan / atau password kosong");
        }

        log.info(mergerText(request.getUsername(), request.getPassword()));
        return ResponseEntity.ok(userService.login(request));
    }

    //    d4c3b2a1
    private String mergerText(String text1, String text2) {
        StringBuilder str1 = new StringBuilder(text1).reverse();
        StringBuilder str2 = new StringBuilder(text2).reverse();
        StringBuilder hasil = new StringBuilder();
        log.info("str1 : {}", str1);
        log.info("str2 : {}", str2);
        int maxLength = Math.max(str1.length(), str2.length());

        for (int i = 0; i < maxLength; i++) {
            if (i < str1.length()) {
                hasil.insert(0,str1.charAt(i));
            }

            if (i < str2.length()) {
                hasil.append(str2.charAt(i));
            }
        }
        return hasil.toString();
    }

}
