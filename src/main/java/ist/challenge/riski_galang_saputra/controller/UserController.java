package ist.challenge.riski_galang_saputra.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import ist.challenge.riski_galang_saputra.entity.User;
import ist.challenge.riski_galang_saputra.model.RegisterRequest;
import ist.challenge.riski_galang_saputra.service.UserService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/*
 * @Author  : galang
 * @Created : 17-11-2023
 * @Email   : riski.saputra@iconpln.co.id
 */
@RestController
@Slf4j
@RequiredArgsConstructor
@RequestMapping("/api/user")
@Api(value = "User API", description = "CRUD User", tags = {"User API"})
public class UserController {

    private final UserService userService;

    @GetMapping
    @ApiOperation(value = "Get All User List")
    public ResponseEntity<?> listUser() {
        return ResponseEntity.ok(userService.getList());
    }

    @PutMapping("/update")
    @ApiOperation(value = "Update data user")
    public ResponseEntity<?> update(@Valid @RequestBody User request) {
        return new ResponseEntity<>(userService.update(request), HttpStatus.CREATED);
    }

    @GetMapping("/get-by-id")
    @ApiOperation(value = "Get user by id")
    public ResponseEntity<?> getUserId(@RequestParam Long id) {
        return ResponseEntity.ok(userService.getUserId(id));
    }

    @PostMapping("/register")
    @ApiOperation(value = "Create new user")
    public ResponseEntity<?> register(@Valid @RequestBody RegisterRequest request) {
        return new ResponseEntity<>(userService.register(request), HttpStatus.CREATED);
    }
}
