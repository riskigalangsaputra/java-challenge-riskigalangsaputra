package ist.challenge.riski_galang_saputra.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import ist.challenge.riski_galang_saputra.service.IntegrationService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/*
 * @Author  : galang
 * @Created : 18-11-2023
 * @Email   : riski.saputra@iconpln.co.id
 */
@RestController
@RequiredArgsConstructor
@Api(value = "Api Integration", description = "Integrasi dengan open api", tags = {"Api Integration"})
public class IntegrasiController {

    private final IntegrationService integrationService;

    @GetMapping("/get-data-people")
    @ApiOperation(value = "List People")
    public ResponseEntity<?> getDataPeople() {
        var data = integrationService.findAllPepople();
        return ResponseEntity.ok(data);
    }
}
