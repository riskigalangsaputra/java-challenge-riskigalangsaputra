package ist.challenge.riski_galang_saputra.config;

/*
 * @Author  : galang
 * @Created : 20-09-2023
 * @Email   : riski.saputra@iconpln.co.id
 */

import ist.challenge.riski_galang_saputra.utils.ConstantUtils;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.web.client.RestTemplate;

@Configuration
@EnableAsync
public class AppConfig {

    @Bean
    public RestTemplate restTemplate() {
        HttpComponentsClientHttpRequestFactory httpRequestFactory = new HttpComponentsClientHttpRequestFactory();
        httpRequestFactory.setConnectionRequestTimeout(ConstantUtils.Setting.CONNECTION_REQUEST_TIMEOUT.value());
        httpRequestFactory.setConnectTimeout(ConstantUtils.Setting.CONNECTION_TIMEOUT.value());
        httpRequestFactory.setReadTimeout(ConstantUtils.Setting.READ_TIMEOUT.value());
        return new RestTemplate(httpRequestFactory);
    }
}
